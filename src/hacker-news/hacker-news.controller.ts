import { Controller, Get, Post, Body, Delete, Put } from '@nestjs/common';
import { CreateHackerNewsDto, DeleteHackerNewsDto, GetHackerNewsDto } from 'src/dto/hacker-news-dto';
import { HackerNewsService } from './hacker-news.service';
import {
    ApiTags,
    ApiOperation
  } from '@nestjs/swagger';

@ApiTags('Hacker News')
@Controller('hacker-news')
export class HackerNewsController {

    constructor(
        private hackerNewsService: HackerNewsService,
      ) { }
    
      @Post('/')
      @ApiOperation({ summary: 'Get Hacker News' })
      async getAllHackersNews(@Body() {author, _tags, title, created_at}: GetHackerNewsDto) {
        return this.hackerNewsService.getAllHackersNews(author, _tags, title, created_at)
      }

      @Post('/save')
      @ApiOperation({ summary: 'Save Hacker News From API' })
      async create() {
        return this.hackerNewsService.saveHackerNewsData()
      }

      @Put('/delete')
      @ApiOperation({ summary: 'Delete New By objectId' })
      async delete(@Body() deleteNews: DeleteHackerNewsDto) {
        return this.hackerNewsService.deleteHackerNews(deleteNews)
      }
}
