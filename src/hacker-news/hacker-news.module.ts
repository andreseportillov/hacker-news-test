import { Module } from '@nestjs/common';
import { HackerNewsController } from './hacker-news.controller';
import { HackerNewsService } from './hacker-news.service';
import { MongooseModule } from '@nestjs/mongoose';
import { HackerNews, HackerNewsSchema } from '../schema/hacker-news.schema';
import { ConfigModule } from '@nestjs/config';
import { HttpModule } from '@nestjs/axios';

@Module({
    imports: [ConfigModule, HttpModule, MongooseModule.forFeature(
        [
            { name: HackerNews.name, schema: HackerNewsSchema }
        ])],
  controllers: [HackerNewsController],
  providers: [HackerNewsService]
})
export class HackerNewsModule {}
