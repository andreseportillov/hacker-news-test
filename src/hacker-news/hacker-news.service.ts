import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { catchError, firstValueFrom } from 'rxjs';
import { Model } from 'mongoose';
import { HackerNews } from '../schema/hacker-news.schema';
import { InjectModel } from '@nestjs/mongoose';
import { DeleteHackerNewsDto } from 'src/dto/hacker-news-dto';
import { Cron } from '@nestjs/schedule';

interface IGetQuery {isRemove: boolean; author: string; _tags: {$in: [string]}; title: string; created_at: string; month: number;}
const monthNames = ["january", "february", "march", "april", "may", "june",
  "july", "august", "september", "october", "november", "december"
];
@Injectable()
export class HackerNewsService {
    constructor(
        @InjectModel(HackerNews.name) private readonly hackerNewsModel: Model<HackerNews>,
        private readonly httpService: HttpService,
    ) { }

    getAllHackersNews = async (author: string, _tags: [string], title: string, created_at: string) => {
        try {
            let query: Partial<IGetQuery> = {isRemove: false}
            if (author && author !== '') {
                query.author = author
            }
            if (_tags && _tags.length > 0 ) {
                query._tags.$in = _tags
            }
            if (title && title !== '') {
                query.title = title
            }
            if (created_at && created_at !== '') {
                const num = monthNames.indexOf(created_at) + 1
                query.month = num
            }
            const aggregate = [
                {$addFields: { month: {$month: '$created_at'}}},
                {$match: query}
            ]
            const hackerNews = await this.hackerNewsModel.aggregate(aggregate)
            return hackerNews
        } catch (e) {
            return e
        }
    }

    getAllHackersNewsFromApi = async () => {
        const path = `${process.env.HACKER_NEWS_URL}`;
        const { data } = await firstValueFrom(
            this.httpService.get<{hits:HackerNews[]}>(path).pipe(
                catchError((error) => {
                    throw 'An error happened!';
                }),
            ),
        );
        return data
    }

    @Cron('0 60 * * *')   
    async saveHackerNewsData () {
        try {
            const listHackerNews = await this.getAllHackersNewsFromApi()
            const hits = listHackerNews.hits
            for (let i = 0; i < hits.length; i++) {
                const hackerNew = hits[i];
                const listHackers = this.hackerNewsModel.findOne({objectID: hackerNew.objectID})
                if (!listHackers) {
                    const hackerNews = new this.hackerNewsModel(hackerNew);
                    await hackerNews.save()
                }

            }
            return listHackerNews
          } catch(e) {
            return e
          }
    }

    async deleteHackerNews (deleteHackerNews: DeleteHackerNewsDto) {
        try {
            if (!deleteHackerNews.objectID) {
                throw new Error("You need to select one objectID of a hacker News");
            }
            await this.hackerNewsModel.updateOne({objectID: deleteHackerNews.objectID}, {$set: {isRemove: true}});
            return true
          } catch(e) {
            return e
          }
    }
}
