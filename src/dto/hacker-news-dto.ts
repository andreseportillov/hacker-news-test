import { ApiProperty } from '@nestjs/swagger';
export class CreateHackerNewsDto {
    @ApiProperty()
    readonly title: string;
    @ApiProperty()
    readonly url: string;
    @ApiProperty()
    readonly id: number;
    @ApiProperty()
    readonly isRemove: boolean;
    @ApiProperty()
    readonly created_at: string;
    @ApiProperty()
    readonly author: string;
    @ApiProperty()
    readonly points: string;
    @ApiProperty()
    readonly story_text: string;
    @ApiProperty()
    readonly comment_text: string;
    @ApiProperty()
    readonly num_comments: string;
    @ApiProperty()
    readonly story_id: number;
    @ApiProperty()
    readonly story_title: string;
    @ApiProperty()
    readonly story_url: string;
    @ApiProperty()
    readonly parent_id: number;
    @ApiProperty()
    readonly created_at_i: number;
    @ApiProperty()
    readonly _tags: [string];
    @ApiProperty()
    readonly objectID: string;
    @ApiProperty()
    readonly _highlightResult: Object;
}

export class GetHackerNewsDto {
    @ApiProperty()
    readonly title: string;
    @ApiProperty()
    readonly created_at: string;
    @ApiProperty()
    readonly author: string;
    @ApiProperty()
    readonly _tags: [string];
}

export class DeleteHackerNewsDto {
    @ApiProperty()
    readonly objectID: string;
}