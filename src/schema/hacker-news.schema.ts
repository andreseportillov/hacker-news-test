import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type HackerNewsDocument = HydratedDocument<HackerNews>;


@Schema()
export class HackerNews {

    @Prop()
    id: number

    @Prop({ type: String, default: null })
    title: string;

    @Prop({ type: String, default: null })
    url: string;

    @Prop({ type: Boolean, default: false })
    isRemove: boolean;

    @Prop()
    created_at: Date;

    @Prop()
    author: string;

    @Prop()
    points: string;

    @Prop()
    story_text: string;

    @Prop()
    comment_text: string;

    @Prop()
    num_comments: string;

    @Prop()
    story_id: number;

    @Prop()
    story_title: string;

    @Prop()
    story_url: string;

    @Prop()
    parent_id: number;

    @Prop()
    created_at_i: number;

    @Prop()
    _tags: [string];

    @Prop()
    objectID: string;

    // @Prop()
    // _highlightResult: {
    //     author: {
    //         value: string,
    //         matchLevel: string,
    //         matchedWords: [string]
    //     }
    // };


}

export const HackerNewsSchema = SchemaFactory.createForClass(HackerNews);