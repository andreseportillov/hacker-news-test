import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { HttpModule } from '@nestjs/axios'; 
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { HackerNewsModule } from './hacker-news/hacker-news.module';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    HttpModule, 
    ScheduleModule.forRoot(),
    ConfigModule.forRoot(), 
    MongooseModule.forRootAsync({
      useFactory: async () => ({
        uri: 'mongodb://127.0.0.1:27017/hackerNewStorage?directConnection=true',
      }),
    }), HackerNewsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
